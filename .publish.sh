#!/bin/sh

release-cli create \
  --name "${1}" \
  --description "${2}" \
  --tag-name "${3}" \
  --ref "${4}" \
  $(for asset in test-*.sh; do echo "--assets-link \"{\\\"name\\\":\\\"${asset}\\\",\\\"url\\\":\\\"${CI_JOB_URL}/artifacts/raw/${asset}\\\"}\""; done) \
  --released-at "$(date -u +%FT%TZ)"
